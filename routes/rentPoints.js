const express = require('express');
const router = express.Router();
const RentPoint = require('../models/rentPoint');
const moment = require('moment');


router.get('/', (req, res) => {
    console.log('Get rent points list')
    
    RentPoint.find({}).exec((error, rentPoints) => {
        if (error) {
            res.status(500).send('Error occured');
        } else {
            console.log(rentPoints);
            res.status(201).send(rentPoints.map(rentPoint => mapToRentPoint(rentPoint)));
        }
        });
    });

    router.post('/', (req, res) => {
        console.log('Post RentPoint');
    
        const newRentPoint = new RentPoint();
    
        newRentPoint.name = req.body.name,
        newRentPoint.city = req.body.city,
        newRentPoint.email = req.body.email,
        newRentPoint.workingHours = req.body.workingHours,
        newRentPoint.phone = req.body.phone,
        newRentPoint.img = req.body.img,
       
        newRentPoint.save((error, rentPoint) => {
           if (error) {
               res.status(500).send('Error occured');
           }
           else {
               res.status(201).send(rentPoint);
               console.log(rentPoint);
           }
        });
    });


    const mapToRentPoint = rentPoint => {
        return {
            id: rentPoint.id,
            name: rentPoint.name,
            city: rentPoint.city,
            email: rentPoint.email,
            workingHours: rentPoint.workingHours,
            phone: rentPoint.phone,
            img: rentPoint.img,
        }
      };


module.exports = router;
