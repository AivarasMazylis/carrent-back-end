const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');

const checkJwt = jwt({
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://mazylis.eu.auth0.com/.well-known/jwks.json`
    }),
  
    audience: 'https://mazylis.eu.auth0.com/.well-known/jwks.json',
    issuer: `https://mazylis.eu.auth0.com/`,
    algorithms: ['RS256']
  });

  const user = ['view:cars']

  const userScopes = jwtAuthz(user);
  const adminScopes = jwtAuthz([...user, 'remove:car', 'create:car' ]);

  module.exports = {checkJwt, userScopes, adminScopes};

