const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rentPointSchema = new Schema ({
    name: String,
    city: String,
    email: String,
    phone: String,
    workingHours: String,
    img: String,
});

module.exports = mongoose.model('RentPoint', rentPointSchema);