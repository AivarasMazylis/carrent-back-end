const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarSchema = new Schema ({
    manufacturer: String,
    model: String,
    fuelType: String,
    gearBoxType: String,
    doorNumber: Number,
    seatNumber: Number,
    extraFeatures: String,
    makeYear: Date,
    bodyWorkType: String,
    rentPointId: String,
    img: String,
});

module.exports = mongoose.model('Car', CarSchema);