const express = require('express');
const nodemailer = require('nodemailer');
const router = express.Router();
const creds = require('../.circleci/config/config');





var transport = {
  host: 'smtp.gmail.com',
  auth: {
    user: creds.USER,
    pass: creds.PASS
  }
}

var transporter = nodemailer.createTransport(transport)

transporter.verify((error, success) => {
  if (error) {
    console.log(error);
  } else {
    console.log('Server is ready to take messages');
  }
});

router.post('/send', (req, res, next) => {
  
  console.log('Send email');
  const name = req.body.name
  const email = req.body.email
  const message = req.body.message
  const reiceverEmail = req.body.reiceverEmail
  const content = `name: ${name} \n email: ${email} \n message: ${message} `

  const mail = {
    from: name,
    to: `${reiceverEmail}`,  //Change to email address that you want to receive messages on
    subject: 'New Message from CarRent Web',
    text: content
  }

  transporter.sendMail(mail, (err, data) => {
    if (err) {
      res.json({
        msg: 'fail'
      })
    } else {
      res.json({
        msg: 'success'
      })
    }
  })
})


module.exports = router;