const express = require('express');
const router = express.Router();
const Car = require('../models/car');
const RentPoint = require('../models/rentPoint');
const moment = require('moment');


router.get('/', (req, res) => {
    console.log('Get car list')

    let query = req.query;
    const seatNumber = req.query['seatNumber'];
    const doorNumber = req.query['doorNumber'];

    if (req.query.seatNumber) {
        query = Object.assign(query, {
          seatNumber: { $gte: seatNumber },
        });
      }

      if (req.query.doorNumber) {
        query = Object.assign(query, {
          doorNumber: { $gte: doorNumber },
        });
      }

    Car.find(query).exec((error, cars) => {
        if (error) {
            res.status(500).send('Error occured');
        } else {
            console.log(cars);
            res.status(201).send(cars.map(car => mapToCar(car)));
        }
        });
    });

    const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };



    router.get('/:id', asyncMiddleware(async (req, res, next) => {
      let car
      try {
        car = await Car.findOne({ _id: req.params.id })
        rentPoint = await RentPoint.findOne({_id: car.rentPointId })
        carRent = {car, rentPoint}
        res.status(200).send(carRent)
      } catch (e) {
        next(e) 
      }
    }));


    router.post('/', (req, res) => {
        console.log('Post Car');
    
        const newCar = new Car();
    
        newCar.manufacturer = req.body.manufacturer,
        newCar.model = req.body.model,
        newCar.fuelType = req.body.fuelType,
        newCar.gearBoxType = req.body.gearBoxType,
        newCar.doorNumber = req.body.doorNumber,
        newCar.seatNumber = req.body.seatNumber,
        newCar.extraFeatures = req.body.extraFeatures,
        newCar.makeYear = req.body.makeYear,
        newCar.bodyWorkType = req.body.bodyWorkType,
        newCar.rentPointId = req.body.rentPointId,
        newCar.img = req.body.img
        
    
        newCar.save((error, car) => {
           if (error) {
               res.status(500).send('Error occured');
           }
           else {
               res.status(201).send(car);
               console.log(car);
           }
        });
    });


    const mapToCar = car => {
        return {
        id: car.id,
        manufacturer: car.manufacturer,
        model: car.model,
        fuelType: car.fuelType,
        gearBoxType: car.gearBoxType,
        doorNumber: car.doorNumber,
        seatNumber: car.seatNumber,
        extraFeatures: car.extraFeatures,
        makeYear: moment(car.makeYear).format("YYYY"),
        bodyWorkType: car.bodyWorkType,
        rentPointId: car.rentPointId,
        img: car.img
        }
      };


module.exports = router;
