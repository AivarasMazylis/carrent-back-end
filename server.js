const express = require('express');
const mongoose = require('mongoose');
const app = express();
var cors = require('cors')
const cars = require('./routes/cars');
const send = require('./routes/send');
const rentPoints = require('./routes/rentPoints');
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

const {checkJwt, userScopes} = require('./helpers/auth');


app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

// database Config

const databaseUser = 'CarRent';
const databasePassword = 'car123';
const databaseName = 'car-rent-project';
const databasePort = '13229';

mongoose.connect(
    `mongodb://${databaseUser}:${databasePassword}@ds213229.mlab.com:${databasePort}/${databaseName}`,
    { useNewUrlParser: true 
    });





app.use(cors());
app.use('/', send);
app.use('/Cars', cars);
app.use('/RentPoints', rentPoints);
app.get('/', (req, res) => res.send('Car Rental'));

app.listen(port, () => 
    console.log(`Bta API application listening on port ${port}!`)
);